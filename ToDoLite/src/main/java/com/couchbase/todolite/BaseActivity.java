package com.couchbase.todolite;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;

import com.couchbase.lite.util.Log;
import com.couchbase.todolite.ruazosa.TooMuchRotationException;

public class BaseActivity extends ActionBarActivity {

    private static final String TAG = Application.TAG;
    private static final String RC = "rotateCount";
    Toolbar toolbar;
    Application application;
    private int rotateCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        application = (Application) getApplication();

        if( savedInstanceState != null ) {
            rotateCount = savedInstanceState.getInt(RC);
            if (rotateCount >= 10) {
                throw new TooMuchRotationException();
            }else{
                rotateCount += 1;
            }
        }
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        setupToolbar();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        android.util.Log.d(Application.TAG, "MainActivity State: onSaveInstanceState()");

        Log.d(TAG, "onSaveInstance rotate count " + rotateCount);
        if( rotateCount > 10 ){
            rotateCount = 0;
        }
        outState.putInt(RC, rotateCount);
    }

    protected void setupToolbar() {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }
    }

    public Toolbar getToolbar() {
        return toolbar;
    }
}
