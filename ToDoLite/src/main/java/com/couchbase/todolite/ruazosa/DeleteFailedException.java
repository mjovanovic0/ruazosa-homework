package com.couchbase.todolite.ruazosa;

/**
 * Created by mjovanovic on 01/04/16.
 */
public class DeleteFailedException extends RuntimeException {
    public DeleteFailedException(){
        super();
    }
}
